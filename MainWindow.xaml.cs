﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Leadtools.Dicom;

namespace proto_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            var licensePath = Properties.Settings.Default.LicenseFilePath;
            var developerKeyPath = Properties.Settings.Default.DeveloperKeyPath;
            var licenseContents = File.ReadAllText(developerKeyPath);
            try
            {
                Leadtools.RasterSupport.SetLicense(licensePath, licenseContents);
                DicomEngine.Startup();
                DicomNet.Startup();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            InitializeComponent();  
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            DicomNet.Shutdown();
            DicomEngine.Shutdown();
        }
    }
}
