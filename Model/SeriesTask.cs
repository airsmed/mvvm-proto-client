﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using proto_client.Utils;
using proto_client.View;
using Leadtools.Dicom;
using Leadtools.Dicom.Scu;

namespace proto_client.Model
{
    public enum SeriesTaskStatus
    {
        [Description("Idle")]
        IDLE = 0,

        [Description("Compress")]
        COMPRESS,

        [Description("Reconstruct")]
        RECONSTRUCT,
        
        [Description("Decompress")]
        DECOMPRESS,

        [Description("Done")]
        DONE, 

        [Description("Cancelled")]
        CANCELLED = 100,

        [Description("Failed")]
        FAILED = 101,
    }

    
    class SeriesTask : INotifyPropertyChanged
    {
        // properties
        private SeriesTaskStatus _status;
        private Series _seriesInfo;
        private List<DicomDataSet> _dicoms;

        public SeriesTask(Series seriesInfo, List<DicomDataSet> dicoms)
        {
            _status = SeriesTaskStatus.IDLE;
            _seriesInfo = seriesInfo;
            _dicoms = dicoms;

        }

        public SeriesTaskStatus Status
        {
            get { return _status; }
            private set { 
                _status = value; 
                OnPropertyChanged("Status");
            }
        }

        public async Task<List<DicomDataSet>> Start(string reconServerUri)
        {
            
            Status = SeriesTaskStatus.COMPRESS;
            var zippedSeries = ZipTool.CompressDicomInSeries(_dicoms);

            Status = SeriesTaskStatus.RECONSTRUCT;
            // var reconBytes = await ApiHelper.SendCompressedSeries(reconServerUri, _seriesInfo.InstanceUID, zippedSeries);
            var reconBytes = zippedSeries;

            Status = SeriesTaskStatus.DECOMPRESS;
            var reconDicoms = ZipTool.DecompressBytesInSeries(reconBytes);

            foreach(var ds in reconDicoms)
            {
                DicomTool.EncodeDataset(ds);
            }
            Status = SeriesTaskStatus.DONE;
            return reconDicoms;
        }

        public void Cancel()
        {
            Status = SeriesTaskStatus.CANCELLED;
        }

        public static string GetStatusDescription(SeriesTaskStatus status)
        {
            FieldInfo fi = status.GetType().GetField(status.ToString());
            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }
            return status.ToString();
        }

        public void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
        public void OnErrorRaised(Exception exception)
        {
            Status = SeriesTaskStatus.FAILED;
            if(ErrorRaised != null)
            {
                ErrorRaised(this, new ErrorEventArgs(exception));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public event ErrorEventHandler ErrorRaised;
    }
}
