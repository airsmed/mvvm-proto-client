﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leadtools.Dicom;
using Leadtools.Dicom.Scu;
using proto_client.Utils;

namespace proto_client.Model
{
    public enum StudyTaskStatus
    {
        [Description("Idle")]
        IDLE = 0,
        
        [Description("Move")]
        MOVE,

        [Description("Reconstruct")]
        RECONSTRUCT,

        [Description("Store")]
        STORE,

        [Description("Done")]
        DONE, 

        [Description("Cancelled")]
        CANCELLED = 100, 

        [Description("Failed")]
        FAILED,
    }

    public class StudyTask : INotifyPropertyChanged
    {
        private StudyTaskStatus _studyTaskStatus;
        private Scu _scu;
        private int _progress;
        private Study _studyInfo;

        public StudyTask(Study studyInfo, StudyTaskStatus studyTaskStatus = StudyTaskStatus.IDLE)
        {
            _studyInfo = studyInfo;
            _progress = 0;
            _studyTaskStatus = studyTaskStatus;
            _scu = new Scu();
        }
        public int Progress
        {
            get { return _progress; }
            private set { _progress = value; OnPropertyChanged("Progress"); }
        }

        public StudyTaskStatus Status
        {
            get { return _studyTaskStatus; }
            private set { _studyTaskStatus = value; OnPropertyChanged("Status"); }
        }

        public Study StudyInfo
        {
            get { return _studyInfo; }
            private set { _studyInfo = value; }
        }

        public async Task Start(PACS pacs, string reconServerUri)
        {
            Status = StudyTaskStatus.MOVE;
            var seriesInfoList = await _scu.CFindSeriesInStudyAsync(pacs, StudyInfo.InstanceUID);
            var datasets = new List<DicomDataSet>[seriesInfoList.Count];
            var reconDicoms = new List<DicomDataSet>();

            var index = 0;
            List<DicomDataSet> dicoms;
            foreach (var seriesInfo in seriesInfoList)
            {
                dicoms = _scu.CMoveDicomsInSeries(pacs, StudyInfo.InstanceUID, seriesInfo.InstanceUID);
                datasets[index ++] = dicoms;
            }
            
            Status = StudyTaskStatus.RECONSTRUCT;
            index = 0;
            foreach (var seriesInfo in seriesInfoList)
            {
                var seriesTask = new SeriesTask(seriesInfo, datasets[index ++]);
                var dcms = await seriesTask.Start(reconServerUri);              
                reconDicoms.AddRange(dcms);
            }

            Status = StudyTaskStatus.STORE;
            var isStored = await _scu.CStoreInSeriesAsync(pacs, reconDicoms);
            if(isStored)
            {
                Status = StudyTaskStatus.DONE;
            } else
            {
                Status = StudyTaskStatus.FAILED;
            }

        }

        public void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
