﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using proto_client.Model;
using proto_client.Utils;
using Leadtools.Dicom;
using Leadtools.Dicom.Scu;
using Leadtools.Dicom.Common;
using Leadtools.Dicom.Scu.Common;
using System.ComponentModel;
using System.Threading;

namespace proto_client.ViewModel
{
    class ReconTaskListViewModel
    {
        // properties
        private StudyTask _selectedStudyTask;
        private string _reconServerUri;
        private PACS _pacs;
        private Scu _scu;
        private bool _isRefreshing;

        public ObservableCollection<StudyTask> StudyTasks
        {
            get; set;
        }

        public StudyTask SelectedStudyTask
        {
            get; set;
        }

        public RelayCommand<object> RefreshCommand
        {
            get; set;
        }

        public RelayCommand<object> BeginReconstructionCommand
        {
            get; set;
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { _isRefreshing = value;  BeginReconstructionCommand.RaiseCanExecuteChanged(); }
        }

        public ReconTaskListViewModel()
        {
            _isRefreshing = false;
            _pacs = SettingManager.LoadPacs();
            _scu = new Scu();
            _reconServerUri = SettingManager.LoadReconServerUri();
            

            StudyTasks = new ObservableCollection<StudyTask>();
            RefreshCommand = new RelayCommand<object>(RefreshStudyTable, CanRefreshStudyTable);
            BeginReconstructionCommand = new RelayCommand<object>(
                BeginReconstruction, CanBeginReconstruction);

        }

        // command
        private async void RefreshStudyTable(object _)
        {
            IsRefreshing = true;
            StudyTasks.Clear();

            var findQuery = new FindQuery();
            var studies = await _scu.CFindStudiesAsync(_pacs, findQuery);
            foreach(var s in studies)
            {
                StudyTasks.Add(new StudyTask(s));
            }
            IsRefreshing = false;
        }

        private bool CanRefreshStudyTable(object _)
        {
            return !IsRefreshing;
        }

        private void BeginReconstruction(object _)
        {
            Task.Run(async () => await SelectedStudyTask.Start(_pacs, _reconServerUri));
            BeginReconstructionCommand.RaiseCanExecuteChanged();
        }

        private bool CanBeginReconstruction(object _)
        {
            return (SelectedStudyTask != null) && (SelectedStudyTask.Status == StudyTaskStatus.IDLE); 
        }  

        // events
        private void MatchStudyFound(object sender, Leadtools.Dicom.Scu.Common.MatchEventArgs<Study> e)
        {
            StudyTasks.Add(new StudyTask(e.Info));
        }

        private void AfterFindFinish(object sender, EventArgs e)
        {
            IsRefreshing = false;
        }
    }
}
