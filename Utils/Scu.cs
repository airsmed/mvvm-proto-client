﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Leadtools.Dicom;
using Leadtools.Dicom.Scu;
using Leadtools.Dicom.Scu.Common;

namespace proto_client.Utils
{
    public class PACS : DicomScp
    {
        public PACS(string aet, string address, int port, int timeout = 0)
        {
            AETitle = aet;
            PeerAddress = IPAddress.Parse(address);
            Port = port;
            // Timeout = timeout;
        }
    }
    public class ProtoStoreScu : StoreScu
    {
        public ProtoStoreScu(string aet, string address, int port, bool useTls = false)
        {
            AETitle = aet;
            HostAddress = IPAddress.Parse(address);
            HostPort = port;
            // TODO: implements TLS option
        }

        protected override void OnReceiveCStoreResponse(byte presentationID, int messageID, string affectedClass, string instance, DicomCommandStatusType status)
        {
            if (status == DicomCommandStatusType.Success)
            {
                CStoreSuccessStatusReceived(this, null);
            }
            base.OnReceiveCStoreResponse(presentationID, messageID, affectedClass, instance, status);
        }

        public event EventHandler CStoreSuccessStatusReceived;
    }

    public class ProtoQueryRetrieveScu : QueryRetrieveScu
    {
        public ProtoQueryRetrieveScu(string aet, string address, int port, string storePath, bool useTls = false)
        {
            AETitle = aet;
            HostAddress = IPAddress.Parse(address);
            HostPort = port;
            UseSecureHost = useTls;

            MaxLength = 46726;
            Flags = DicomNetFlags.None;
            EnableDebugLog = true;
        }
    }
    class Scu
    {
        static readonly ProtoQueryRetrieveScu _queryRetrieveScu = SettingManager.LoadQueryRetrieveScu();
        static readonly ProtoStoreScu _storeScu = SettingManager.LoadStoreScu();

        private List<Study> _studies;
        private List<Series> _series;
        private List<DicomDataSet> _images;

        private AutoResetEvent _autoResetEvent;

        private int _storeSuccessCount;
        
        public Scu()
        {
            _studies = new List<Study>();
            _series = new List<Series>();
            _images = new List<DicomDataSet>();
            _autoResetEvent = new AutoResetEvent(false);
            _queryRetrieveScu.AfterClose += this.AfterClose;
            _storeSuccessCount = 0;
        }

        public List<Study> CFindStudies(DicomScp scp, FindQuery findQuery)
        {
            _studies.Clear();
            _queryRetrieveScu.MatchStudy += this.MatchStudyFound;
            _queryRetrieveScu.Find(scp, findQuery);
            _autoResetEvent.WaitOne();
            _queryRetrieveScu.MatchStudy -= this.MatchStudyFound;
            return _studies.ToList<Study>();
        }

        public Task<List<Study>> CFindStudiesAsync(DicomScp scp, FindQuery findQuery)
        {
            return Task.Run(() => CFindStudies(scp, findQuery));
        }

        public List<Series> CFindSeriesInStudy(DicomScp scp, string studyUid)
        {
            _series.Clear();
            _queryRetrieveScu.MatchSeries += this.MatchSeriesFound;
            FindQuery findQuery = new FindQuery();
            findQuery.StudyInstanceUID = studyUid;
            findQuery.QueryLevel = QueryLevel.Series;
            
            _queryRetrieveScu.Find(scp, findQuery);
            _autoResetEvent.WaitOne();
            _queryRetrieveScu.MatchSeries -= this.MatchSeriesFound;
            return _series.ToList<Series>();  
        }

        public Task<List<Series>> CFindSeriesInStudyAsync(DicomScp scp, string studyUid)
        {
            return Task.Run(() => CFindSeriesInStudy(scp, studyUid));
        }

        public List<DicomDataSet> CMoveDicomsInSeries(DicomScp scp, string studyUid, string seriesUid)
        {
            _images.Clear();
            _queryRetrieveScu.EnableMoveToSelf = true;
            _queryRetrieveScu.Moved += this.Moved;
            _queryRetrieveScu.Move(scp, _queryRetrieveScu.AETitle, studyUid, seriesUid);
            _autoResetEvent.WaitOne();
            _queryRetrieveScu.Moved -= this.Moved;
            return _images.ToList<DicomDataSet>();
        }

        
        public Task<List<DicomDataSet>> CMoveDicomsInSeriesAsync(DicomScp scp, string studyUid, string seriesUid)
        {
            return Task.Run(() => CMoveDicomsInSeries(scp, studyUid, seriesUid));
        }

        public bool CStoreInSeries(DicomScp scp, List<DicomDataSet> dss)
        {
            _storeSuccessCount = 0;
            _storeScu.CStoreSuccessStatusReceived += CStoreSuccessReceived;
            _storeScu.AfterClose += AfterClose;
            foreach (var ds in dss)
            {
                _storeScu.Store(scp, ds);
            }
            _autoResetEvent.WaitOne();
            _storeScu.AfterClose -= AfterClose;
            _storeScu.CStoreSuccessStatusReceived -= CStoreSuccessReceived;
            return dss.Count == _storeSuccessCount;
        }

        public Task<bool> CStoreInSeriesAsync(DicomScp scp, List<DicomDataSet> dss)
        {
            return Task.Run(() => CStoreInSeries(scp, dss));
        }

        private void MatchStudyFound(object sender, MatchEventArgs<Study> e)
        {
            _studies.Add(e.Info);
        }

        private void MatchSeriesFound(object sender, MatchEventArgs<Series> e)
        {
            _series.Add(e.Info);
        }

        private void Moved(object sender, MovedEventArgs e)
        {
           _images.Add(e.Dataset);
        }

        private void AfterClose(object sender, EventArgs e)
        {
            _autoResetEvent.Set();
        }

        public void CStoreSuccessReceived(object sender, EventArgs e)
        {
            _storeSuccessCount += 1;
        }
    }
}
