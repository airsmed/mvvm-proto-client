﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace proto_client.Utils
{
    class ApiHelper
    {
        static readonly HttpClient client = new HttpClient();
        public static async Task<byte[]> SendCompressedSeries(string serverUri, string postId, byte[] zippedData)
        {
            // TODO: handling http exceptions
            // string uri = $"{serverUri}/{postId}";
            string uri = serverUri;
            var form = new MultipartFormDataContent();
            form.Add(new ByteArrayContent(zippedData, 0, zippedData.Length), "data", "temp.zip");
            // form.Add(new StringContent(postId), "id");       
            HttpResponseMessage response = await client.PostAsync(uri, form);
            response.EnsureSuccessStatusCode();          
            client.Dispose();
            byte[] reconBytes = await response.Content.ReadAsByteArrayAsync();
            return reconBytes;
        }
    }
}
