﻿using System;


namespace proto_client.Utils
{
    public class SettingManager
   { 
        public static string LoadReconServerUri()
        {
            var uri = new UriBuilder(Properties.Settings.Default.ReconServerAddress);
            uri.Port = Properties.Settings.Default.ReconServerPort;
            return uri.ToString();
        }

        public static PACS LoadPacs()
        {
            return new PACS(
                Properties.Settings.Default.PACSAet,
                Properties.Settings.Default.PACSAddress,
                Properties.Settings.Default.PACSPort,
                Properties.Settings.Default.PACSTimeOut
            );
        }

        public static ProtoStoreScu LoadStoreScu()
        {
            return new ProtoStoreScu(
                Properties.Settings.Default.ScuAet,
                Properties.Settings.Default.ScuAddress,
                Properties.Settings.Default.ScuPort,
                Properties.Settings.Default.ScuUseTls
            );
        }

        public static ProtoQueryRetrieveScu LoadQueryRetrieveScu()
        {
            return new ProtoQueryRetrieveScu(
                Properties.Settings.Default.ScuAet,
                Properties.Settings.Default.ScuAddress,
                Properties.Settings.Default.ScuPort,
                Properties.Settings.Default.ScuStorePath,
                Properties.Settings.Default.ScuUseTls
                );
        }     
    }
}
