﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using Leadtools.Dicom;
using Leadtools.Dicom.Scu;
using Leadtools.Dicom.Scu.Common;
using System.Threading;

namespace proto_client.Utils
{
    class DicomTool
    {
        static string UidPrefix = "2.25.";
        public static bool FilterSeries()
        {
            return true;
        }

        public static void Anonymize(DicomDataSet ds)
        {

        }


        public static bool IsEncodedUid(string uid)
        {
            return uid.StartsWith(UidPrefix);
        }

        public static string EncodeUid(string originalUid)
        {
            return UidPrefix + originalUid;
        }

        public static string DecodeUid(string sfmrUid)
        {
            if(IsEncodedUid(sfmrUid))
            {
                return sfmrUid.Substring(UidPrefix.Length);
            }
            return sfmrUid;
        }

        public static string GetStringValueByTag(DicomDataSet ds, long tagValue)
        {
            var element = ds.FindFirstElement(null, tagValue, true);
            var value = ds.GetValue<string>(element, "");
            return value;
        }
        public static void EncodeDataset(DicomDataSet ds)
        {
            var studyUidElement = ds.FindFirstElement(null, DicomTag.StudyInstanceUID, true);
            var seriesUidElement = ds.FindFirstElement(null, DicomTag.SeriesInstanceUID, true);
            var instanceUidElement = ds.FindFirstElement(null, DicomTag.SOPInstanceUID, true);

            var newSopUid = EncodeUid(ds.GetStringValue(instanceUidElement, 0));
            var newSeriesUid = EncodeUid(ds.GetStringValue(seriesUidElement, 0));
            var newStudyUid = EncodeUid(ds.GetStringValue(studyUidElement, 0));

            ds.InsertElementAndSetValue(DicomTag.StudyInstanceUID, newStudyUid);
            ds.InsertElementAndSetValue(DicomTag.SeriesInstanceUID, newSeriesUid);
            ds.InsertElementAndSetValue(DicomTag.SOPInstanceUID, newSopUid);
        }

        public static bool IsEncodedDataset(DicomDataSet ds)
        {
            var studyUidElement = ds.FindFirstElement(null, DicomTag.StudyInstanceUID, true);
            var studyUid = ds.GetStringValue(studyUidElement, 0);
            return IsEncodedUid(studyUid);
        }

    }
}
