﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using Leadtools.Dicom;

namespace proto_client.Utils
{
    class ZipTool
    {
        public static byte[] CompressDicomInSeries(List<DicomDataSet> dicoms)
        {
            byte[] compressedBytes = null;
            
            using(var outStream = new MemoryStream())
            {
                using(var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    int index = 0;
                    foreach( var dcm in dicoms)
                    {
                        var fileInArchive = archive.CreateEntry($"{index}.dcm", CompressionLevel.Optimal);
                        using (var entryStream = fileInArchive.Open())
                        {
                            using (var memoryToCompression = new MemoryStream())
                            {
                                dcm.Save(memoryToCompression, DicomDataSetSaveFlags.None);
                                memoryToCompression.CopyTo(entryStream);
                            }
                        }
                        index += 1;
                    }
                }
                compressedBytes = outStream.ToArray();
            }
            return compressedBytes;
        }

        public static List<DicomDataSet> DecompressBytesInSeries(byte[] reconBytes)
        {
            List<DicomDataSet> dicoms = new List<DicomDataSet>();
            using(var inputStream = new MemoryStream(reconBytes))
            {
                using (var archive = new ZipArchive(inputStream, ZipArchiveMode.Read))
                {
                    var index = 0;
                    foreach (var entry in archive.Entries)
                    {
                        using(var entryStream = entry.Open())
                        {
                            var ds = new DicomDataSet();
                            using (var dicomStream = new MemoryStream())
                            {
                                var filepath = Properties.Settings.Default.ScuStorePath + $"\\{index++}.dcm";
                                entryStream.CopyTo(dicomStream);
                                dicomStream.Seek(0, SeekOrigin.Begin);
                                ds.Load(dicomStream, DicomDataSetLoadFlags.None);
                                dicoms.Add(ds);
                            }
                        }
                    }
                }
            }
            return dicoms;
        }
    }
}
